<?php

class netim2 extends DomainModule {

    protected $description = 'Netim2 Domain Registrar module';
    protected $configuration = [
        'UserID' => [
            'value' => '',
            'type' => 'input',
            'default' => false
        ],
        'Password' => [
            'value' => '',
            'type' => 'input',
            'default' => false
        ],
        'test' => [
            'value' => '',
            'type' => 'check'
        ]
    ];

    protected $commands = array(
        'Register', 'Transfer', 'Renew', 'ContactInfo', 'DNSManagement', 'EmailForwarding', 'RegisterNameServers', 'EppCode'
    );

    private $id_session = 0;

    public function createConnection(){

        if($this->configuration['test'])
            $url = "http://oteapi.netim.com/2.0/api.wsdl";
        else
            $url = "http://api.netim.com/2.0/api.wsdl";

        $clientSOAP = new SoapClient($url);


        try
        {
            $id_session = $clientSOAP->sessionOpen($this->configuration['UserID']['value'], $this->configuration['Password']['value'], "EN");

        }
        catch(SoapFault $fault)
        {
            $this->addError("Exception : " .$fault->getMessage());
            return false;
        }



        $this->setIdSession($id_session);

        return $clientSOAP;
    }


    public function testConnection(){

        if($this->createConnection())
            return true;

        return false;

    }


    public function checkDomainAvailable($connection){

        try
        {
            $response = $connection->domainCheck($this->getIdSession(), $this->options['sld'].".".$this->options['tld']);

        }
        catch(SoapFault $fault)
        {
            $this->addError("Exception : " .$fault->getMessage());
            return false;
        }

        $info = $response[0];

        if($info->reason!='1' OR in_array($response->result, ['NOT AVAILABLE', 'UNKNOWN'])){
            $this->addError('Domain availability: '.$response->result.' '.$response->reason);
            return false;
        }

        return true;

    }


    public function Register() {


        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        if(!$this->checkDomainAvailable($connection)){
            $this->addError('Error: domain is no longer available');
            return false;
        }



        $id_owner = $this->createContact($connection, $this->domain_contacts['registrant'], 1);
        $id_admin = $this->createContact($connection, $this->domain_contacts['admin'], 0);
        $id_tech = $this->createContact($connection, $this->domain_contacts['tech'], 0);
        $id_billing = $this->createContact($connection, $this->domain_contacts['registrant'], 0);

        if(!$id_owner || !$id_admin || !$id_tech){
            $this->addError('Error: unable to create contact');
            return false;
        }

        try {
            $response = $connection->domainCreate($this->getIdSession(), $this->options['sld'].".".$this->options['tld'], $id_owner, $id_admin, $id_tech, $id_billing, $this->options["ns1"], $this->options["ns2"] ? $this->options["ns2"] : "", $this->options["ns3"] ? $this->options["ns3"] : "", $this->options["ns4"] ? $this->options["ns4"] : "", $this->options["ns5"] ? $this->options["ns5"] : "", $this->options['numyears']);
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());
            return false;
        }


        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('STATUS: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }

        $this->addInfo($response->STATUS);
        if($response->STATUS == 'Done')
            $this->addDomain('Active');
        else
            $this->addDomain('Pending Registration');

        $connection->sessionClose($this->getIdSession());
        return true;

    }



    public function Renew() {
        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        try{
            $response= $connection->domainRenew($this->getIdSession(), $this->options['sld'].".".$this->options['tld'], $this->options['numyears']);
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());
            return false;
        }

        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('STATUS: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }

        $this->addInfo($response->STATUS);
        $this->addPeriod();
        $connection->sessionClose($this->getIdSession());
        return true;

    }




    public function Transfer() {

        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        $id_owner = $this->createContact($connection, $this->domain_contacts['registrant'], 1);

        try{
            $response = $connection->domainTransferOwner($this->getIdSession(), $this->options['sld'].".".$this->options['tld'], $id_owner);
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());
            return false;
        }

        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('STATUS: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }

        $this->addInfo($response->STATUS);
        $this->addPeriod();
        $connection->sessionClose($this->getIdSession());
        return true;

    }


    public function getEppCode() {
        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        try {
            $response = $connection->domainAuthID($this->getIdSession(), $this->options['sld'].".".$this->options['tld'], 1);
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());
            return false;
        }

        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('STATUS: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }



        $this->addInfo('Epp code has been sent on email address');
        $connection->sessionClose($this->getIdSession());
        return true;
    }


    public function synchInfo() {

        $domain = $this->domainInfo();


        if(!$domain)
            return false;


        $info = [];
        $info['expires'] = $domain->dateExpiration;
        $info['reglock'] = $domain->domainIsLock ? true : false;


        switch($domain->status){
            case 'PENDING':
                $info['status'] = 'Pending';
                break;
            case 'ERROR':
                $info['status'] = 'Cancelled';
                break;
            case 'ACTIVE, NOT DELEGATED':
                $info['status'] = 'Active';
                break;
            case 'ACTIVE, DELEGATED':
                $info['status'] = 'Active';
                break;
            case 'RESERVED, NOT DELEGATED':
                $info['status'] = 'Pending';
                break;
            case 'EXPIRED':
                $info['status'] = 'Expired';
                break;
            case 'EXPIRED, HOLD, NOT DELEGATED':
                $info['status'] = 'Expired';
                break;
            case 'QUARANTINE, NOT DELEGATED':
                $info['status'] = 'Expired';
                break;
            case 'HOLD, NOT DELEGATED':
                $info['status'] = 'Pending';
                break;

        }

        foreach($domain->ns AS $ns){
            $info['ns'][] = $ns;
        }


        return $info;
    }

    public function domainInfo(){
        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }


        try {
            $response = $connection->domainInfo($this->getIdSession(), $this->options['sld'].".".$this->options['tld']);
        }
        catch(SoapFault $fault) {
            $this->addError($fault->getMessage());
            return false;
        }

        $connection->sessionClose($this->getIdSession());
        return $response;
    }

    public function updateNameServers(){
        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }


        try {
            $response = $connection->domainChangeDNS($this->getIdSession(), $this->options['sld'].".".$this->options['tld'], $this->options["ns1"], $this->options["ns2"] ? $this->options["ns2"] : "", $this->options["ns3"] ? $this->options["ns3"] : "", $this->options["ns4"] ? $this->options["ns4"] : "", $this->options["ns5"] ? $this->options["ns5"] : "");
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());
            return false;
        }

        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('STATUS: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }

        $connection->sessionClose($this->getIdSession());
        return true;


    }

    public function createContact($connection, $contact, $owner = 0){

        $contactArray = $this->prepareContactInfo($contact, $owner);

        try{
            $idContact = $connection->contactCreate($this->getIdSession(), $contactArray);
        }
        catch(SoapFault $fault){
            $this->addError($fault->getMessage());

            return false;
        }

        return $idContact;

    }

    public function updateContactInfo(){

        $domain = $this->domainInfo();

        if(!$domain){
            $this->addError('Cannot find domain: '.$this->options['sld'].".".$this->options['tld']);
            return false;
        }

        $id_owner   = $domain->idOwner;
        $ownerArray = $this->prepareContactInfo($this->domain_contacts['registrant'], 1);

        $id_admin   = $domain->idAdmin;
        $adminArray = $this->prepareContactInfo($this->domain_contacts['admin'], 0);

        $id_tech    = $domain->idTech;
        $techArray = $this->prepareContactInfo($this->domain_contacts['tech'], 0);

        $id_billing = $domain->idBilling;
        $billingArray = $this->prepareContactInfo($this->domain_contacts['registrant'], 0);


        if($this->updateContact($id_owner, $ownerArray) AND $this->updateContact($id_admin, $adminArray) AND $this->updateContact($id_tech, $techArray) AND $this->updateContact($id_billing, $billingArray))
            return true;
        else{
            $this->addError('Contact update error');
            return false;
        }



    }

    public function updateContact($id_contact, $contactArray){
        $connection = $this->createConnection();

        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        try
        {
            $response = $connection->contactUpdate($this->getIdSession(), $id_contact, $contactArray);

        }
        catch(SoapFault $fault)
        {
            $this->addError($fault->getMessage());
            return false;
        }

        if($response->MESSAGE OR !in_array($response->STATUS, ['Done', 'Pending'])){
            $this->addError('Contact update error: '.$response->STATUS.' '.$response->MESSAGE);
            return false;
        }


        $connection->sessionClose($this->getIdSession());

        return true;

    }

    public function addAdditionalInfo($hbInfo, $owner){
        $tld = $this->options['tld'];

        $other = [];
        $additional = [];

        switch($tld){
            case 'aero':
                $additional['AERO_AUTHID'] =  $this->options['ext']['AERO_AUTHID'];
                $additional['AERO_AUTHKEY'] = $this->options['ext']['AERO_AUTHKEY'];
                break;
            case 'asn.au':
                $additional['AU_REGISTRANT_ID_NUMBER'] = [$this->options['sld'].'.com.au' => $this->options['ext']['AU_REGISTRANT_ID_NUMBER']];
                $additional['AU_ELIGIBILITY_TYPE'] = [$this->options['sld'].'.com.au' => $this->options['ext']['AU_ELIGIBILITY_TYPE']];
                break;
            case 'bg':
                $additional['BG_LEGAL_REP_FIRSTNAME'] = $this->options['ext']['BG_LEGAL_REP_FIRSTNAME'];
                $additional['BG_LEGAL_REP_LASTNAME'] = $this->options['ext']['BG_LEGAL_REP_LASTNAME'];
                break;
            case 'ca':
                $additional['CA_LEGAL_TYPE'] = $this->options['ext']['CA_LEGAL_TYPE'];
                if(!in_array($this->options['ext']['CA_LEGAL_TYPE'], ['TDM', 'CCT'])){
                    $other['country'] = 'CA';
                    $other['area'] = $this->options['ext']['area'];
                }
                break;
            case 'cat':
                $additional['CAT_INTENDED_USE'] = [$this->options['sld'].'.cat'    => $this->options['ext']['CAT_INTENDED_USE']];
                break;
            case 'co.hu':
                if($hbInfo['company'] AND in_array($hbInfo['country'], ['AT', 'BE', 'BG', 'CZ', 'CY', 'DE', 'DK', 'ES', 'EE', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LI', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK']))
                    $other['vatNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'com.au':
                $additional['AU_REGISTRANT_ID_NUMBER'] =[$this->options['sld'].'.com.au' =>  $this->options['ext']['AU_REGISTRANT_ID_NUMBER']];
                $additional['AU_REGISTRANT_ID_TYPE'] =[$this->options['sld'].'.com.au' =>  $this->options['ext']['AU_REGISTRANT_ID_TYPE']];
                $additional['AU_ELIGIBILITY_TYPE'] =[$this->options['sld'].'.com.au' =>  $this->options['ext']['AU_ELIGIBILITY_TYPE']];
                break;
            case 'com.cy':
                $other['country'] = 'CY';
                if($hbInfo['company'])
                    $other['vatNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];

                break;
            case 'com.my':
                $other['country'] = 'CY';
                $other['bodyForm'] = 'ORG';
                $other['companyNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'com.sg':
                if($owner)
                    $other['bodyForm'] = 'ORG';
                else {
                    $other['bodyForm'] = 'IND';
                    $other['country'] = 'SG';
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                }
            case 'dk':
                if($hbInfo['company'] AND in_array($hbInfo['country'], ['AT', 'BE', 'BG', 'CZ', 'CY', 'DE', 'DK', 'ES', 'EE', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LI', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK']))
                    $other['vatNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'ee':
                if($owner AND $hbInfo['company']){
                    $other['bodyForm'] = 'ORG';
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                }
                else{
                    $other['bodyForm'] = 'IND';
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'es':
                $additional['ES_TIPO'] = $this->options['ext']['ES_TIPO'];
                if($this->options['ext']['ES_TIPO']==0)
                    $additional['ES_OTHER'] = $this->options['ext']['identification_value'];
                elseif($this->options['ext']['ES_TIPO']==1)
                    $additional['ES_NIF'] = $this->options['ext']['identification_value'];
                else
                    $additional['ES_NIE'] = $this->options['ext']['identification_value'];
                break;
            case 'fi':
                if($hbInfo['company'])
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                elseif($hbInfo['country']=='FI')
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['birthDate'] = $this->options['ext']['birth_date'];
                break;
            case 'hr':
                if($hbInfo['country']=='HR')
                    $additional['HR_OIB'] = $this->options['ext']['identification_value'];
                elseif(in_array($hbInfo['country'], ['AT', 'BE', 'BG', 'CZ', 'CY', 'DE', 'DK', 'ES', 'EE', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LI', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'])){
                    $other['bodyForm'] = 'ORG';
                    $other['vatNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'hu':
                if($owner) {
                    if ($hbInfo['company'])
                        $other['vatNumber'] = $this->options['ext']['identification_value'];
                    else
                        $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                else
                    $other['country'] = 'HU';
                break;
            case 'id.au':
                $other['country'] = 'AU';
                $other['bodyForm'] = 'IND';
                break;
            case 'ie':
                if($hbInfo['company']){
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                }
                else
                    $other['country'] = 'IR';

                if(in_array($hbInfo['country'], ['IE', 'GB', 'US']))
                    $other['area'] = $this->options['ext']['state'];
                break;
            case 'is':
                if($hbInfo['country']=='IS'){
                    if($hbInfo['company'])
                        $other['companyNumber'] =$this->options['ext']['identification_value'];
                    else
                        $other['idNumber'] =$this->options['ext']['identification_value'];
                }
                break;
            case 'it':
                if($hbInfo['country']=='IT'){
                    $other['area'] = $this->options['ext']['state'];
                    if($owner){
                        if($hbInfo['company'])
                            $other['vatNumber'] = $this->options['ext']['identification_value'];
                        else
                            $other['idNumber'] = $this->options['ext']['identification_value'];
                    }
                }
                break;
            case 'jobs':
                $other['bodyForm'] = 'ORG';
                $additional = $this->options['ext']['identification_value'];
                break;
            case 'kr':
                $other['country'] = 'KR';
                $additional['KR_DOC'] = $this->options['ext']['KR_DOC'];
                if($hbInfo['company'])
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'lv':
                if($owner AND $hbInfo['country']=='LV'){
                    if($hbInfo['company'])
                        $other['vatNumber'] = $this->options['ext']['identification_value'];
                    else
                        $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'mx':
                if(in_array($hbInfo['country'], ['MX', 'US']))
                    $other['area'] = $this->options['ext']['identification_value'];
                break;
            case 'my':
                $other['country'] = 'MY';
                if($hbInfo['company'])
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'net.au':
                $additional['AU_REGISTRANT_ID_NUMBER'] =[$this->options['sld'].'.net.au' =>  $this->options['ext']['AU_REGISTRANT_ID_NUMBER']];
                $additional['AU_REGISTRANT_ID_TYPE'] =[$this->options['sld'].'.net.au' =>  $this->options['ext']['AU_REGISTRANT_ID_TYPE']];
                $additional['AU_ELIGIBILITY_TYPE'] =[$this->options['sld'].'.net.au' =>  $this->options['ext']['AU_ELIGIBILITY_TYPE']];
                break;
            case 'no':
                $other['country'] = 'NO';
                if($hbInfo['company'])
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'nu':
                if($hbInfo['company']){
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                    if(in_array($hbInfo['country'], ['AT', 'BE', 'BG', 'CZ', 'CY', 'DE', 'DK', 'ES', 'EE', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LI', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK']))
                        $other['vatNumber'] = ['AU_REGISTRANT_ID_NUMBER'=>[$this->options['sld'].'.com.au' => ''], ''=>''];
                }
                else{
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                }

                break;
            case 'org.au':
                $additional['AU_REGISTRANT_ID_NUMBER'] =[$this->options['sld'].'.org.au' =>  $this->options['ext']['AU_REGISTRANT_ID_NUMBERAU_REGISTRANT_ID_NUMBER']];
                $additional['AU_ELIGIBILITY_TYPE'] =[$this->options['sld'].'.org.au' =>  $this->options['ext']['AU_ELIGIBILITY_TYPE']];
                break;
            case 'pro':
                $additional = $this->options['ext']['profession'].'#'.$this->options['ext']['certification'].'#'.$this->options['ext']['adress'].'#'.$this->options['ext']['license'];
                break;
            case 'ro':
                $other['country'] = 'RO';
                if($hbInfo['company']) {
                    $other['vatNumber'] = $this->options['ext']['vat_value'];
                    $other['companyNumber'] = $this->options['ext']['id_value'];
                }
                else
                    $other['idNumber'] = $this->options['ext']['id_value'];
                break;
            case 'ru':
                if($hbInfo['company']) {
                    $other['companyNumber'] = $this->options['ext']['company_number'];
                }
                else {
                    $other['birthDate'] = $this->options['ext']['birth_date'];
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'se':
                if($hbInfo['company']) {
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                    if(in_array($hbInfo['country'], ['AT', 'BE', 'BG', 'CZ', 'CY', 'DE', 'DK', 'ES', 'EE', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LI', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK']))
                        $other['vatNumber'] = $this->options['ext']['vat_value'];
                }
                else
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'sg':
                if($owner){
                    if($hbInfo['company'])
                        $other['companyNumber'] = $this->options['ext']['identification_value'];
                    else
                        $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                else{
                    $other['country'] = 'SG';
                    $other['bodyForm'] = 'IND';
                    $other['idNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'sk':
                if($hbInfo['company'])
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                break;
            case 'swiss':
                if($owner){
                    $other['bodyForm'] = 'ORG';
                    $other['country'] = 'CH';
                    $other['companyNumber'] = $this->options['ext']['identification_value'];
                    $additional['SWISS_INTENDED_USE'] = [$this->options['sld'].'.swiss' => $this->options['ext']['SWISS_INTENDED_USE']];
                }
                break;
            case 'ua':
                if($owner){
                    $additional['UA_TRADEMARK_NAME'] = [$this->options['sld'].'.ua' => $this->options['ext']['UA_TRADEMARK_NAME']];
                    $additional['UA_TRADEMARK_NUMBER'] = $this->options['ext']['UK_CATEGORY'];
                    if(in_array($this->options['ext']['UK_CATEGORY'], ['LTD', 'PLC', 'LLP', 'IP', 'SCH', 'RCHAR']))
                        $other['companyNumber'] = $this->options['ext']['identification_value'];
                }
                break;
            case 'uk':
                if($owner AND in_array($hbInfo['country'], ['GB', 'UK'])){
                    $additional['UK_CATEGORY'] = $this->options['ext']['UA_TRADEMARK_NUMBER'];

                }
                else{
                    $other['country'] = 'GB, JE, GG, IM';
                }
                break;
            case 'us':
                $additional = ['AU_REGISTRANT_ID_NUMBER'=>[$this->options['sld'].'.com.au' => ''], ''=>''];
                break;

            default:
                $additional = [];
                break;
        }

        return ['additional' => $additional, 'other' => $other];
    }

    public function prepareContactInfo($hbInfo, $owner) {

        $structContact = [];
        $structContact["firstName"] = $hbInfo["firstname"];
        $structContact["lastName"] = $hbInfo["lastname"];
        if($hbInfo['company']) {
            $structContact["bodyForm"] = 'ORG';
            $structContact["bodyName"] = $hbInfo['companyname'];
        }
        else {
            $structContact["bodyForm"] = "IND";
            $structContact["bodyName"] = '';
        }

        $structContact["address1"] = $hbInfo["address1"];
        $structContact["address2"] = $hbInfo["address2"];
        $structContact["zipCode"] = $hbInfo["postcode"];
        $structContact["area"] = $hbInfo["country"]=="IT" ? $hbInfo["state"] : "";
        $structContact["city"] = $hbInfo["city"];
        $structContact["country"] = $hbInfo["country"];
        $structContact["phone"] = $hbInfo["phonenumber"];
        $structContact["fax"] = "";
        $structContact["email"] = $hbInfo["email"];
        $structContact["language"] = "EN";
        $structContact["isOwner"] = $owner;
        $structContact["tmName"] = "";
        $structContact["tmNumber"] = "";
        $structContact["tmType"] = "";
        $structContact["tmDate"] = "";
        $structContact["companyNumber"] = "";
        $structContact["vatNumber"] = "";
        $structContact["birthDate"] = "";
        $structContact["birthZipCode"] = "";
        $structContact["birthCity"] = "";
        $structContact["birthCountry"] = "";
        $structContact["idNumber"] = "";

        if(in_array(strtoupper($this->options['tld']), ['AERO', 'ASN.AU', 'BG', 'CA', 'CAT', 'CO.HU', 'COM.AU', 'COM.CY', 'COM.MT', 'COM.MY', 'COM.SG', 'DK', 'EE', 'ES', 'FI', 'HR', 'HU', 'ID.AU', 'IE', 'IS', 'IT', 'JOBS', 'KR', 'LV', 'MT', 'MX', 'MY', 'NET.AU', 'NO', 'NU', 'ORG.AU', 'PRO', 'RO', 'RU', 'SE', 'SG', 'SK', 'SWISS', 'UA', 'UK', 'US'] )){
            $additionalInfo = $this->addAdditionalInfo($hbInfo, $owner);

            $structContact["additional"] = $additionalInfo['additional'];
            $structContact = array_merge($structContact, $additionalInfo['other']);
        }
        else {
            $structContact["additional"] = array();
        }

        return $structContact;
    }

    public function getExtendedAttributesV2() {
        $baseTld = '.' . end(explode('.', $this->options['tld'], 2));
        $ext = json_decode(file_get_contents(__DIR__ . DS . 'extended.json'), true);


        if (isset($ext[$baseTld])) {
            foreach ($ext[$baseTld] as $k => $field) if (isset($field['items']) && $field['items'] == 'list_contirues') {
                $tmp = Utilities::get_countries(true);
                $ext[$baseTld][$k]['items'] = [];
                foreach ($tmp as $cc => $name) $ext[$baseTld][$k]['items'][] = ['variable_id' => $cc,
                    'name' => $name];
            }

            return [$baseTld => $ext[$baseTld]];
        }

        return [];
    }



    public function widget_dnssec_form()
    {
        return [
            'type' => ['KEY', 'DS']
        ];
    }

    public function widget_dnssec_get()
    {
        $domain = $this->domainInfo();

        if(!$domain)
            return false;

        $records = [];
        foreach ($domain->DNSSEC as $sec) {
            if($sec['type']==KEY) {

                $records[] = [

                    'id' => md5($sec['Algorithm'] . $sec['Public Key']),
                    'type' => 'KEY',
                    'flag' => $sec['Flags'],
                    'algorithm' => $sec['Algorithm'],
                    'protocol' => $sec['Protocol'],
                    'publickey' => $sec['Public Key'],
                ];
            }
            else{
                $records[] = [

                    'id' => md5($sec['Algorithm'] . $sec['Digest']),
                    'type' => 'DS',
                    'key_tag' => $sec['KeyTag'],
                    'algorithm' => $sec['Algorithm'],
                    'digest_type' => $sec['DigestType'],
                    'digest' => $sec['Digest'],

                ];
            }
        }
        return $records;

    }

    public function widget_dnssec_set($data)
    {
        if(!isset($data['add']) OR !$data['add']){
            $this->addError('Data error');
            return false;
        }

        $flags = $data['add']['flag'];
        $alg = $data['add']['algorithm'];
        $protocol = $data['add']['protocol'];
        $publicKey = $data['add']['publickey'];


        $DSRecords = [
            'key'       => $data['add']['key_tag']? $data['add']['key_tag'] : 1,
            'type'      => $data['add']['digest_type']? $data['add']['digest_type'] : 3,
            'digest'    => $data['add']['digest']? $data['add']['digest'] : '1234'
        ];


        $connection = $this->createConnection();


        if(!$this->getIdSession()) {
            $this->addError('Connection failed');
            return false;
        }

        try
        {
            $response = $connection->domainSetDNSSecExt($this->getIdSession(), $this->options['sld'].'.'.$this->options['tld'], $DSRecords, $flags, $protocol, $alg, $publicKey);

            $connection->sessionClose($this->getIdSession());


        }
        catch(SoapFault $fault)
        {
            $this->addError('Exception : ' .$fault->getMessage());
            return false;

        }

        return true;

    }



    public function setIdSession($id){
        $this->id_session = $id;
    }

    public function getIdSession(){
        return $this->id_session;
    }

}